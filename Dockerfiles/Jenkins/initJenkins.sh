#!/usr/bin/env bash

docker build -t jenkins-docker .

docker run -d --name jenkins-pipeline -p 8010:8080 -v ~/jenkins_home:/bitnami --env JAVA_OPTS="-Xmx2048m -Djava.awt.headless=true" --link sonarqube:sonarqube jenkins-docker
