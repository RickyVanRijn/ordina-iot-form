import hudson.plugins.git.*;
import jenkins.model.*;
import hudson.triggers.SCMTrigger;
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.impl.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

//println("Setting git credentials")
//
//def domain = Domain.global()
//def store = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()
//
//def gitCredentialsUsername = new File("/etc/secret-volume/gitUsername").text.trim()
//def gitCredentialsPassword = new File("/etc/secret-volume/gitPassword").text.trim()
//
//def credentials=['username' : gitCredentialsUsername, 'password' : gitCredentialsPassword, 'description' : 'User Git Credentials']
//def user = new UsernamePasswordCredentialsImpl(CredentialsScope.GLOBAL, 'User_Credentials', credentials.description, credentials.username, credentials.password)
//
//store.addCredentials(domain, user)

println("Installing tools")
mavenName = "Maven 3.3.9"
mavenVersion = "3.3.9"
println("Checking Maven installations...")

// Grab the Maven "task" (which is the plugin handle).
mavenPlugin = Jenkins.instance.getExtensionList(hudson.tasks.Maven.DescriptorImpl.class)[0]

// Check for a matching installation.
maven3Install = mavenPlugin.installations.find {
    install -> install.name.equals(mavenName)
}

// If no match was found, add an installation.
if(maven3Install == null) {
    println("No Maven install found. Adding...")

    newMavenInstall = new hudson.tasks.Maven.MavenInstallation('Maven 3.3.9', null,
            [new hudson.tools.InstallSourceProperty([new hudson.tasks.Maven.MavenInstaller(mavenVersion)])]
    )

    mavenPlugin.installations += newMavenInstall
    mavenPlugin.save()

    println("Maven install added.")
} else {
    println("Maven install found. Done.")
}

println("Setting up workflow...")

def scm = new GitSCM("https://gitlab.com/RickyVanRijn/ordina-iot-form.git")
scm.branches = [new BranchSpec("*/master")];

def flowDefinition = new org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition(scm, "Jenkinsfile")
//flowDefinition.scm.userRemoteConfigs[0].credentialsId = 'User_Credentials'

def parent = Jenkins.instance
def job = new org.jenkinsci.plugins.workflow.job.WorkflowJob(parent, "Ordina Buildpack")
job.definition = flowDefinition

def spec = "*/5 * * * *";
SCMTrigger newCron = new SCMTrigger(spec);
newCron.start(job, true);
job.addTrigger(newCron);
job.save();

parent.reload()

//println InetAddress.localHost.getByName('sonar').getHostAddress()
//println InetAddress.localHost.getHostAddress()
