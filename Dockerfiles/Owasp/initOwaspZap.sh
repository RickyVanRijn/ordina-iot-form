#!/usr/bin/env bash

docker run -d -u zap -p 8090:8080 -i owasp/zap2docker-bare zap.sh -daemon -host 0.0.0.0 -port 8080 -config api.addrs.addr.name=.* -config api.addrs.addr.regex=true -config api.key=exampleKey
