#!/usr/bin/env bash

docker run -d --name sonarqube --restart always -p 9000:9000 sonarqube
