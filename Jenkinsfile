pipeline {
    agent any

    tools {
        maven 'Maven 3.3.9'
        jdk 'jdk8' //requires manual step in Jenkins -> Global Tool Configuration -> JDK -> JDK installations
    }

    stages {
        stage('Build'){
            steps{
                sh 'mvn -Dmaven.repo.local=/opt/bitnami/jenkins/jenkins_home/.m2/repository clean install -DskipTests=true'
            }
        }
        stage('Test') {
            steps {
                sh 'mvn -Dmaven.repo.local=/opt/bitnami/jenkins/jenkins_home/.m2/repository test'
            }
        }
        stage('Code Quality') {
            steps {
                sh 'mvn -s conf/settings.xml -Dsonar.userHome=/opt/bitnami/jenkins/jenkins_home/.sonar/cache sonar:sonar'
            }
        }
        stage('Build & Test Security') {
            steps {
                script {
                    sh "mvn verify -Dmaven.repo.local=/opt/bitnami/jenkins/jenkins_home/.m2/repository -Dhttp.proxyHost=192.168.1.106 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=192.168.1.106 -Dhttps.proxyPort=8080" // Proxy tests through ZAP
                    sh 'curl http://192.168.1.106:8080/JSON/alert/view/alerts/?apikey=owasp -o zapResults.json'
                }
            }
        }
        stage('Deploy') {
            steps {
                sh 'mvn -Dmaven.repo.local=/opt/bitnami/jenkins/jenkins_home/.m2/repository clean compile assembly:single'
            }
        }
    }

    post {
        always {
            archiveArtifacts artifacts: 'target/*.jar',  fingerprint: true
            archiveArtifacts artifacts: 'zapResults.json', onlyIfSuccessful: true
        }
    }
}
