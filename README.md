# Ordina Iot Form
This java project is meant for an one hour session of hands-on creating an automated software project.

The first thing you would wish to do is check out the code. This repository is therefore public and can be cloned by everyone. 

The following steps are taken in de hands-on session
- Checking out a Java Software Project with the Spring Framework
- Start a Docker container running Jenkins
- Start a Docker container running SonarQube
- Start a Docker container running Owasp ZAP Tool
- Start a Docker container with your Java Software

The following steps are not shown in the session but are needed for a working situation.
- Configure the applications

# What is Git
Git is a version control management which is today the standard for versioning your software.

# What is Maven
Maven is a dependency manager for Java projects. You can add/delete/manage you dependencies with this.

# What is Docker
Docker is a technology which consists of a virtual system which undependently runs with his own OS Environment variables and installation. This way you can run an application in an isolated environment but for example still on your laptop.

# What is Jenkins?
Jenkins is a pipeline application used for automated software deployments and other software related tasks like testing.
A Pipeline is a sequence of tasks which are automatically performed and when one of them fails (configurable) the pipeline fails.
This could mean by example that if tests fail, your application isn't functioning well enough to be deployed. 

The pipeline needs to be triggered to run. This is an user defined action which is usually configured to be triggered when an commit has been pushed to a branch.

# What is SonarQube
When you're in a software project there are usually a set of rules/conditions which you will establish before you start the project.
SonarQube is a tool which checks your code based on rulesets and code language. This is used for code quality. The default rulesets which are provided by SonarQube are based on the default conventions used by the current code language.

# What is Owasp ZAP tool
OWASP is a term which you should be hearing often but many of you know it as an awareness session. Few know that OWASP has a number of tools which can provide a basic security scan.
For example, the OWASP ZAP Tool is an scanning tool which scans your webapplication from the outside and checks whether there are possible security exploits which needs revision.

# Configuring the applications

## Jenkins 
Most important: Is is needed for the Jenkins pipeline to know where to code is to perform the build and test process. Therefore i provided a Jenkinsfile which consists of a already configured Jenkins pipeline.
The user credentials and git repo are filled with dummy values, you need to change them if you want a working pipeline.

Jenkins is in this case the centre of your configuration. The following needs to be set:
- Git credentials
- SonarQube url
- OWASP Zap url and credentials

## SonarQube
SonarQube needs to have the code language of Java ruleset installed and a token needs to be created so Jenkins can access the SonarQube server.

## Owasp ZAP Tool
This needs to have a updated database with CVE's for the security scan and user credentials which are also needed for Jenkins to access the OWASP ZAP server.

# Needed in advance for performing this session by yourself

- Install Git, see https://git-scm.com/book/en/v2/Getting-Started-Installing-Git.
- Install OpenJDK, see https://adoptopenjdk.net/installation.html.
- Install Maven, see https://maven.apache.org/install.html.
- Install Docker, see https://docs.docker.com/install/.
- Install an IDE, free examples are Eclipse or IDEA Intellij Community Edition.

