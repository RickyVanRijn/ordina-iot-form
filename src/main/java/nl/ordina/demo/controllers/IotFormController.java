package nl.ordina.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IotFormController {

    @GetMapping("/form")
    public String  getForm() {
        return "<html><head><title>Demo</title></head><body><h1>Hello World</h1></body></html>";
    }
}
