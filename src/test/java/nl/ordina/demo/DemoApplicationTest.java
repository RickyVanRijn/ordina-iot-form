package nl.ordina.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DemoApplication.class)
class DemoApplicationTest {

    @Test
    public void contextLoads() {
    }

    @Test
    public void test_context() {
        DemoApplication.main(new String[]{"--server.port=9999"});
    }

}
