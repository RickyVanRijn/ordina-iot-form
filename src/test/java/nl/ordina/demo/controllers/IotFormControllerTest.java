package nl.ordina.demo.controllers;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class IotFormControllerTest {

    private IotFormController sut = new IotFormController();

    private final String OUTPUT_FORM = "<html><head><title>Demo</title></head><body><h1>Hello World</h1></body></html>";

    @Test
    void getForm() {
        assertEquals(sut.getForm(), OUTPUT_FORM);
    }
}
